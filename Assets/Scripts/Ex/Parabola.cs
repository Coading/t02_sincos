﻿using UnityEngine;

public class Parabola : MonoBehaviour
{
    public Transform m_Target;
    public float m_Speed = 10;
    public float m_HeightArc = 1;
    private Vector3 m_StartPosition;
    private bool m_IsFire;

    void Start()
    {
        m_StartPosition = transform.position;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_IsFire = true;
        }

        if (m_IsFire)
        {
            float sx = m_StartPosition.x;
            float sy = m_StartPosition.y;
            float ex = m_Target.position.x;
            float ey = m_Target.position.y;

            float distance = ex - sx;
            float x = Mathf.MoveTowards(this.transform.position.x, ex, m_Speed * Time.deltaTime);
            float y = Mathf.Lerp(sy, ey, (x - sx) / distance);
            float arc = m_HeightArc * (x - sx) * (x - ex) / (-0.25f * distance * distance);
            
            Vector3 position = new Vector3(x, y + arc, transform.position.z);
            transform.rotation = LookAt2D(position - transform.position);
            transform.position = position;

            if (position == m_Target.position)
                Arrived();
        }
    }

    void Arrived()
    {
        m_IsFire = false;
        this.transform.position = m_StartPosition;
    }

    Quaternion LookAt2D(Vector2 forward)
    {
        return Quaternion.Euler(0, 0, Mathf.Atan2(forward.y, forward.x) * Mathf.Rad2Deg);
    }
}