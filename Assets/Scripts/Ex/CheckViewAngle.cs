﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckViewAngle : MonoBehaviour
{
    public float ViewAngle = 90;// 시야 각
    public Transform target; // 범위안에 있는지 없는지 체크할 오브젝트 (이하 적 이라고 표현함)
    private long debug_count;
    private SpriteRenderer spr;

    private void Start()
    {
        spr = target.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        Vector3 F = this.transform.up; // 나의 시선 벡터
        Vector3 A = (target.transform.position - transform.position).normalized; // 적을 향한 방향 벡터

        // F와 A 벡터 내적을 계산한다
        float dot = Vector3.Dot(F, A);

        // F와 A벡터의 사이각을 계산한다 thetha = cos^-1 (F dot A / |F||A|)
        // F와 A 벡터는 모두 단위 벡터 이므로 내적값은 두 벡터가 이루는 각의 Cos 값과 같다
        float degree = Mathf.Acos(dot) * Mathf.Rad2Deg;

        if (degree <= ViewAngle * 0.5f) {
            // 시야 범위 안에 있다
            spr.color = Color.white;
        } else  {
            // 시야 범위 밖에 있다
            spr.color = new Color32(255, 255, 255, 70);
        }

        ColBoundary();
        Drawline();
    }

    private Vector3 m_VecLeft;
    private Vector3 m_VecRight;
    private void ColBoundary()
    {
        m_VecLeft = RotationVector2D(transform.up, ViewAngle / 2);
        m_VecRight = RotationVector2D(transform.up, -ViewAngle / 2);
    }

    public Vector2 RotationVector2D(Vector2 v, float theta)
    {
        float radian = theta * Mathf.Deg2Rad;

        return new Vector2(
            v.x * Mathf.Cos(radian) - v.y * Mathf.Sin(radian),
            v.x * Mathf.Sin(radian) + v.y * Mathf.Cos(radian)
        );
    }

    private void Drawline()
    {
        Debug.DrawLine(transform.position, transform.position + m_VecLeft * 10, Color.blue);
        Debug.DrawLine(transform.position, transform.position + m_VecRight * 10, Color.blue);
    }
}
