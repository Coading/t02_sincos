﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCosX : MonoBehaviour
{
    public float length = 1f;
    public float speed = 1f;

    float time = 0f;
    float pos_x;

    private void Awake()
    {
        pos_x = transform.position.x;
    }

    public void Update()
    {
        Vector3 pos = transform.position;

        time += Time.deltaTime * speed;

        pos.x = pos_x;
        pos.x += Mathf.Cos(time) * length;

        transform.position = pos;
    }
}
