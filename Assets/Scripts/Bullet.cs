﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Vector3 direction;
    public float speed = 3f;

    void Update()
    {
        transform.position += direction * speed * Time.deltaTime;
        OutOfScreen();
    }

    void OutOfScreen()
    {
        Vector2 viewportPos = 
		Camera.main.WorldToViewportPoint(transform.position);

        if (viewportPos.x > 1 || viewportPos.x < 0 || 
		viewportPos.y > 1 || viewportPos.y < 0) {
            Destroy(gameObject);
        }
    }
}

