﻿using System.Collections;
using UnityEngine;

public class Enemy : BattleShip
{
    public GameObject prefabHit;
    public GameObject prefabDestroy;

    public void CreateHitFX()
    {
        if (prefabHit != null)
        {
            GameObject.Instantiate(prefabHit, transform.position, Quaternion.identity);
        }
    }

    public void CreateDestroyFX()
    {
        if (prefabDestroy != null)
        {
            GameObject.Instantiate(prefabDestroy, transform.position, Quaternion.identity);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (hp < 0)
            return;

        Destroy(other.gameObject);

        hp--;

        if (hp <= 0)
        {
            CreateDestroyFX();
            Destroy(this.gameObject);
        }
        else
        {
            CreateHitFX();
        }
    }
}