﻿using UnityEngine;
using System.Collections;

public class FireTargetNWay : MonoBehaviour
{
    public GameObject prefabBullet;
    public float delay = 0.2f;
    public Transform target;

    //탄환과 탄환 사이의 각도
    [SerializeField]
    private float delta_angle = 10f;

    //탄환의 수
    [Range(1,60)]
    public int count = 1;

    public bool pause = false;

    void Start()
    {
        StartCoroutine(FireLoop());
    }

    IEnumerator FireLoop()
    {
        while (target != null)
        {
            if (pause == false)
            {
                Vector2 dir = target.position - transform.position;

                OnFire(dir);
            }

            yield return new WaitForSeconds(delay);
        }
    }

    public Vector2 RotationVector2D(Vector2 v, float theta)
    {
        float radian = theta * Mathf.Deg2Rad;

        return new Vector2(
            v.x * Mathf.Cos(radian) - v.y * Mathf.Sin(radian),
            v.x * Mathf.Sin(radian) + v.y * Mathf.Cos(radian)
        );
    }

    public void OnFire(Vector3 target_dir)
    {
        float start_angle;
        //짝수
        if (count % 2 == 0)
        {
            start_angle = (-count / 2 + 0.5f) * delta_angle;
        }
        //홀수
        else
        {
            start_angle = -(count - 1) / 2 * delta_angle;
        }

        for (int i = 0; i < count; i++)
        {
            Vector3 dir = RotationVector2D(target_dir, start_angle);
            start_angle += delta_angle;

            FireAngle.CreateBullet(prefabBullet, transform.position, dir);
        }
    }
}