﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSinY : MonoBehaviour
{
    public float length = 1f;
    public float speed = 1f;

    float time = 0f;
    float pos_y;

    private void Awake()
    {
        pos_y = transform.position.y;
    }

    public void Update()
    {
        Vector3 pos = transform.position;

        time += Time.deltaTime * speed;

        pos.y = pos_y;
        pos.y += Mathf.Sin(time) * length;

        transform.position = pos;
    }
}
