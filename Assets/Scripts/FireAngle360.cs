﻿using UnityEngine;
using System.Collections;

public class FireAngle360 : MonoBehaviour
{
	public GameObject prefabBullet;
	public float delay = 0.5f;

	[Range(0, 50)]
	public int count = 20;
	public bool pause = false;

	void Start()
	{
		StartCoroutine(FireLoop());
	}

	IEnumerator FireLoop()
	{
		while (true)
		{
			if (pause == false)
			{
				for (int i = 0; i < count; i++)
				{
					float theata = (Mathf.PI * 2) / count * i;
					Vector2 dir = new Vector3(Mathf.Cos(theata), Mathf.Sin(theata));

					FireAngle.CreateBullet(prefabBullet, transform.position, dir);
				}
			}

			yield return new WaitForSeconds(delay);
		}
	}
}
