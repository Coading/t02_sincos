﻿using UnityEngine;
using System.Collections;

public class FireAngle : MonoBehaviour
{
	public GameObject prefabBullet;
	public float delay = 0.2f;
	
	[Range(0,360)]
	public float angle = 0f;
	public float velocity_rotation = 0f;
	public bool pause = false;

	void Start()
	{
		StartCoroutine(FireLoop());
	}

	IEnumerator FireLoop()
	{
		while (true)
		{
			if (pause == false)
			{
				angle += velocity_rotation;

				float theta = angle * Mathf.Deg2Rad;
				Vector2 dir = new Vector3(Mathf.Cos(theta), Mathf.Sin(theta));			

				CreateBullet(prefabBullet, transform.position, dir);
			}

			yield return new WaitForSeconds(delay);
		}
	}

	public static Bullet CreateBullet (GameObject prefab, Vector3 pos, Vector3 direction)
    {
		if (direction == Vector3.zero)
			return null;

		GameObject go = Instantiate(prefab) as GameObject;
		Bullet bullet = go.GetComponent<Bullet>();
		bullet.transform.position = pos;
		bullet.direction = direction.normalized;
		return bullet;
	}
}
