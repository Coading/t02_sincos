﻿using UnityEngine;
using System.Collections;

public class FireMissile : MonoBehaviour
{
	public Transform target;
	public GameObject prefabBullet;
	[Range(0, 50)]
	public int count = 20;
	public float delay = 0.2f;

	public bool pause = false;

	void Start()
	{
		StartCoroutine(FireLoop());
	}

	IEnumerator FireLoop()
	{
		while (true)
		{
			if (pause == false)
			{
				for (int i = 0; i < count; i++)
				{
					float theata = (Mathf.PI * 2) / count * i;
					Vector2 dir = new Vector3(Mathf.Cos(theata), Mathf.Sin(theata));

					CreateMissile(prefabBullet, target, this.transform.position, dir);
				}
			}

			yield return new WaitForSeconds(delay);
		}
	}

	public static Missile CreateMissile(GameObject prefab, Transform _target, Vector3 _pos, Vector3 _dir)
	{
		if (_target == null)
			return null;

		GameObject go = Instantiate(prefab) as GameObject;
		Missile missile = go.GetComponent<Missile>();
		missile.Init(_target, _pos, _dir);
		return missile;
	}
}
