﻿using UnityEngine;
using System.Collections;

public class FireForward : MonoBehaviour
{
	public GameObject prefabBullet;
	public float delay = 0.2f;
	
	public bool pause = false;

	void Start()
	{
		StartCoroutine(FireLoop());
	}

	IEnumerator FireLoop()
	{
		while (true)
		{
			if (pause == false)
			{
				FireAngle.CreateBullet(prefabBullet, transform.position, transform.up);
			}

			yield return new WaitForSeconds(delay);
		}
	}
}
