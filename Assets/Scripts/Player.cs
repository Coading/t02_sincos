﻿using System.Collections;
using UnityEngine;

public class Player : BattleShip
{
    public GameObject prefabHit;

    public void CreateHitFX()
    {
        if (prefabHit != null)
        {
            GameObject.Instantiate(prefabHit, transform.position, Quaternion.identity);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
        {
            Vector3 dir = (enemy.transform.position - transform.position).normalized;
            float dot = Vector3.Dot(transform.up, dir);
            if (dot < 0) // 벡터의 내적값이 음수일때 두 벡터의 방향이 같다 (적이 뒷쪽에 있다)
            {
                // 후면충돌
                GameObject.Instantiate(prefabHit, enemy.transform.position, Quaternion.identity);
            }

            hp--;
            if (hp <= 0) {
                hp = 0;
                Debug.Log("Game Over");
            }
            Destroy(enemy.gameObject);
        }
    }
}