﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCenter : MonoBehaviour
{
    public float speed = 1f;

    public void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, 
            Vector3.zero, speed * Time.deltaTime);
    }
}
