﻿using UnityEngine;

public class Missile : MonoBehaviour
{
    public Transform m_target;
    public float velocity = 8f;
    public float accuracy = 3f;// 정확도
    public float time_tracker = 0.5f; // 추적시간
    private float timer; 
    private Vector3 direction;
    private Vector3 foward;
    float rot_z;


    public void Init (Transform target, Vector3 pos, Vector3 dir)
    {
        m_target = target;

        this.transform.position = pos;

        rot_z = Quaternion.FromToRotation(Vector3.up, dir).eulerAngles.z;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, rot_z));        

        timer = Time.time + time_tracker;
    }

    void Update()
    {
        transform.position += transform.up * velocity * Time.deltaTime;

        if (m_target != null && Time.time < timer) {
            rot_z = Quaternion.FromToRotation(Vector3.up, m_target.position - this.transform.position).eulerAngles.z;
        }

        Quaternion q = Quaternion.Euler(new Vector3(0, 0, rot_z));
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime * accuracy);
        OutOfScreen();
    }

    void OutOfScreen()
    {
        Vector2 viewportPos = 
		Camera.main.WorldToViewportPoint(transform.position);

        if (viewportPos.x > 1 || viewportPos.x < 0 || viewportPos.y > 1 || viewportPos.y < 0) {
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(this.transform.position, transform.up * 0.5f);
    }
}

