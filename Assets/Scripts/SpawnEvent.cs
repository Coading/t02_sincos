﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEvent : MonoBehaviour
{
    public GameObject prefabEnemy;

    void Update()
    {
        if (Input.GetMouseButtonUp(1) == true)
        {
            var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = transform.position.z;
            GameObject.Instantiate(prefabEnemy, pos, Quaternion.identity);
        }
    }
}
