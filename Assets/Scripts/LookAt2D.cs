﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt2D : MonoBehaviour
{
	public GameObject target;
	public float rotSpeed = 1f;

	float GetAngle(Vector2 start,Vector2 target)
	{
		Vector2 dt = target - start;
		float rad  =  Mathf . Atan2 (dt.y , dt.x);
		float degree = rad * Mathf.Rad2Deg;

		if (degree < 0)  {			
			degree += 360; // Atan2 결과값 -180 ~ 180 를 360도로 변환한다
		}

		return degree;
	}

	private void Update()
    {	
		if (target != null)
        {
			var rot_z = 0f;

#if _QUATERNION_
			rot_z = GetAngle(this.transform.position, target.transform.position);
			rot_z -= 90; // up 벡터를 -90도 회전시킨다
#else
			/*
             * 쿼터니언 사용 예시 (-90 대신 회전축을 지정할 수 있다)
            */
			rot_z = Quaternion.FromToRotation(Vector3.up, target.transform.position - this.transform.position).eulerAngles.z;
#endif

			Quaternion rotation = Quaternion.Euler(new Vector3(0, 0, rot_z));
			transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotSpeed * Time.deltaTime);		
		}
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(this.transform.position, transform.up * 10f);
    }
}
